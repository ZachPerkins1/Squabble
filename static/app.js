$URL_REGEX = /(https?:\/\/[^\s\*]+)/g;
$BOLD_REGEX = /\*\S+\*/g
$ITALIC_REGEX = /_\S+_/g

var isActive = true;
var manager;

function setCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function getCookie(name, def) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    
    setCookie(name, def, 365)
    return def;
}

function getBooleanCookie(name, def) {
    if (!def) 
        def = true;
    var curr = getCookie(name, def? "1": "0");
    return curr == "1";
}

function setBooleanCookie(name, value) {
    setCookie(name, value? "1": "0", 365);
}

function eraseCookie(name) {
    setCookie(name, "", -1);
}

$(document).ready(function() {
    $("#message-wrapper").resizable({
        handles: 'n'
    });
    
    manager = createMessageManager();
    manager.originalTitle = document.title;
    manager.loadMore();
})

window.onbeforeunload = function(event) {
    if (getCookie("sq_localcontroller", $LOCAL_ID.toString()) == $LOCAL_ID.toString())
        setCookie("sq_localcontroller", -1);
}

window.onfocus = function () { 
    isActive = true;
    manager.resetNotify();
    setBooleanCookie("sq_tabbedin", true);
}; 

window.onblur = function () { 
    isActive = false;
    setBooleanCookie("sq_tabbedin", false);
}; 


// Simple ajax query to send a message
// Probably should do this with POST instead

var keys = [];

window.onkeyup = function(e) {keys[e.keyCode]=false;}
window.onkeydown = function(e) {keys[e.keyCode]=true;}

function keyPressed() {
    if(event.keyCode == 13) {
        if (!keys[16]) {
            manager.sendMessage();
            event.preventDefault();
        }
    }
}

function roughlyEqual(n1, n2) {
    var EPSILON = 1;
    
    var lower = n1 - EPSILON;
    var upper = n1 + EPSILON;
    return n2 < upper && n2 > lower;
}

function createMessageManager() {
    var m = {
        currentMessageID: 0, //Keeps track of the current message ID
        // important when polling for messages
        messageBlocks: [],
        pendingMessages: [],
        newMessages: 0,
        originalTitle: null,
        BUFFER: 30, // Amount of messages to store in one block rec. >= 50
        //TODO: update this buffer amount to always fit the screen size
        scrollLocked: true, // Indicates whether the scrollbar is being locked to the newest message
        bufferLocked: true, // Indicates whether or not the chat is locked to message block 0
        moreToLoad: true,
        currentlyLoading: false,
        scrolling: false,
        unloadTimer: null,
        notifSound: document.getElementById("notification-audio"),
        
        shouldNotify: function() {
            var localString = $LOCAL_ID.toString();
            
            var userSetting = getBooleanCookie("sq_chatnotify", true);
            var controller = getCookie("sq_localcontroller", localString);
            var tabbedIn = getBooleanCookie("sq_tabbedin", true);
            
            if (controller == "-1") {
                setCookie("sq_localcontroller", localString);
                controller = localString
            }
            
            return userSetting && controller == localString && !tabbedIn;
        },
        
        notify: function() {
            if (!isActive) {
                this.newMessages++;
                var str = this.newMessages.toString();
                if (this.newMessages > 99)
                    str = "99+"
                    
                document.title = "(" + str + ") " + this.originalTitle;
            }
            
            if (this.shouldNotify())
                this.notifSound.play();
        },
        
        resetNotify: function() {
            document.title = this.originalTitle;
            this.newMessages = 0;
        },
        
        // Processes a scroll and updates our buffer lock and scroll lock variables. Called on any scroll event.
        processScroll: function() {
            if (!this.scrolling) {
                // When the scrollbar hits the bottom
                if (roughlyEqual($("#chat-wrapper").scrollTop(), $('#chat-wrapper').prop('scrollHeight') - $('#chat-wrapper').innerHeight())) {
                    this.scrollLocked = true;
                    if (!this.bufferLocked) {
                        //if (this.unloadTimer != null)
                        //    clearTimeout(this.unloadTimer)
                            
                        //this.unloadTimer = setTimeout(this.unloadMore, 50000); // Unload the extra bits after 50 seconds
                        this.unloadMore();
                        
                    }
                } else {
                    this.scrollLocked = false;
                    this.bufferLocked = false;
                }
                
                // When the scrollbar hits the top
                if ($("#chat-wrapper").scrollTop() == 0) {
                    this.loadMore();
                }
            }
            
            this.scrolling = false;
        },
        
        doScroll: function(amt) {
            this.scrolling = true;
            $("#chat-wrapper").scrollTop(amt);
        },
        
        poll: function() {
            $.ajax({ url: "/poll-messages",
            data: {
                local_id: $LOCAL_ID,
                c_index: manager.currentMessageID
            },
            error: function(){
                manager.poll();
            },      
            success: function(data) {
                manager.processMessages(data.messages.reverse());
                manager.poll();
            }, dataType: "json", type: "POST", timeout: 50000});
        },
        
        // Loads BUFFER more messages into another message block
        loadMore: function() {
            console.log(this.currentlyLoading)
            if (!this.currentlyLoading && this.moreToLoad) {
                this.currentlyLoading = true;
                var c_index;
                if (this.messageBlocks.length > 0)
                    c_index = manager.messageBlocks[manager.messageBlocks.length - 1][manager.messageBlocks[manager.messageBlocks.length - 1].length - 1].id
                else
                    c_index = -1;
                    
                // Query server for BUFFER more messages
                $.ajax({ url: "/retrieve-messages",
                    data: {
                        chat_id: 1,
                        c_index: c_index, //the id of the oldest rendered message
                        size: manager.BUFFER + 1 // Request one more than buffer to see if any more available in this chat
                    },
                    error: function(){
                        //TODO: this
                    },      
                    success: function(data) {
                        //Add the new message block
                        manager.messageBlocks.push([]);
                        
                        //See if there are more messages to load
                        manager.moreToLoad = data.messages.length > manager.BUFFER;
                        
                        var st = $("#chat-wrapper").scrollTop();
                        var topIndex = Math.min(data.messages.length, manager.BUFFER);
                    
                        // Populate our buffer with the new messages
                        for (var i = 0; i < topIndex; i++) {
                            var m = data.messages[i];
                            var m_o = createMessage(m.message, m.sender, m.sent, m.uid);
                            manager.messageBlocks[manager.messageBlocks.length - 1].push(m_o);
                            $("#text-window").prepend(m_o.node)
                        }
                        
                        var message_nodes = $("#chat-wrapper").find(".chat");

                        manager.currentlyLoading = false;
                        if (manager.messageBlocks.length == 1) {
                            if (manager.messageBlocks[0].length > 0)
                                manager.currentMessageID = manager.messageBlocks[0][0].id;
                            manager.poll()
                        }
                            
                        manager.doScroll(message_nodes.eq(topIndex).position().top);

                    }, dataType: "json", type: "POST", timeout: 50000});
            }
        },
        //Unloads the extra bits: confines it down to only the first message_block
        unloadMore: function() {
            function removeMessageNode(message, _, _) {
                $(message.node).remove(); 
            }
            
            while (manager.messageBlocks.length > 1) {
                var block = manager.messageBlocks.pop();
                block.forEach(removeMessageNode);
            }
            
            var extra = manager.messageBlocks[0].slice(manager.BUFFER, manager.messageBlocks[0].length)
            extra.forEach(removeMessageNode);
                
            this.doScroll($('#chat-wrapper').prop('scrollHeight') - $('#chat-wrapper').innerHeight() + 2);
            manager.messageBlocks[0] = manager.messageBlocks[0].slice(0, manager.BUFFER);
            manager.bufferLocked = true;
            manager.moreToLoad = true;
        },
        addMessage: function(message) {
            this.messageBlocks[0].unshift(message);
            $("#text-window").append(message.node);
            if (this.bufferLocked && this.messageBlocks[0].length > this.BUFFER) {
                this.moreToLoad = true;
                $(this.messageBlocks[0][this.BUFFER].node).remove();
                this.messageBlocks[0].pop();
            }
            
            if (this.scrollLocked)
                this.doScroll($('#chat-wrapper').prop('scrollHeight') - $('#chat-wrapper').innerHeight() + 2);
        },
        // Processes the messages from our poll function
        // TODO: - Make the ajax query part of this method
        //       - Get rid of reverse nonsense
        processMessages: function(messages) {
            if (this.shouldNotify() && messages.length > 0 && !isActive) {
                this.notify();
            }
            
            for (var i = 0; i < messages.length; i++) {
                var m = messages[i];
                var m_o = createMessage(m.message, m.sender, m.sent, m.uid);
                this.addMessage(m_o);
                
            }
            
            if (messages.length > 0) 
                this.currentMessageID = messages[messages.length - 1].uid;
        },
        sendMessage: function() {
            var text = document.getElementById("message").value;
            if (text.length > 0) {
                var message = createMessage(text, $USER, Date());
                this.addMessage(message);
        
                $.ajax({ url: "/send-message",
                    data: {
                        message: text,
                        local_id: $LOCAL_ID
                    },
                    error: function() {
                        // This is where we need to show the message send error
                    },
                    success: function(data) {
                        var cID = data["c_id"];
                        message.verify(cID);
                        if (cID > manager.currentMessageID)
                            manager.currentMessageID = cID;
                    }
                });
            }

            document.getElementById("message").value = "";
        }
    }
    
    return m;
}

// A simple constructor for a message object
// Essentially just a wrapper for a message 
function createMessage(message, sender, timestamp, id) {
    if (!id)
        id = -1;
    
    var msg = {
        message: message,
        sender: sender,
        timestamp: new Date(timestamp),
        id: id,
        verified: true,
        node: null,
        // Creates the javascript node that will be inserted into the page when the message is rendered
        create_node: function() {
            var div = document.createElement("div");
            var spacer = document.createElement("div");
            var message = document.createElement("p");
            
            if (this.sender == $USER) {
                div.className = "chat you";
            } else {
                div.className = "chat them";
            }
            
            if (!this.verified)
                div.className = "unverified " + div.className;
            
            message.className = "chatmessage";
            spacer.className = "spacer";

            div.appendChild(spacer);
            var inserted = this.message.replace(/\n/g, "<br/>");
            
            inserted = inserted.replace($BOLD_REGEX, function(text) {
                return '<b>' + text.substring(1, text.length - 1) + '</b>';
            });
            inserted = inserted.replace($ITALIC_REGEX, function(text) {
                return '<i>' + text.substring(1, text.length - 1) + '</i>';
            });
            inserted = inserted.replace($URL_REGEX, function(url) {
                return '<a href="' + url + '" target="_blank">' + url + '</a>';
            });
            
            $(message).html(inserted);
            div.appendChild(message);
            
            
            this.node = div;
        },
        verify: function(id) {
            if (!this.verified) {
                this.verified = true;
                this.id = id;
                this.node.className = this.node.className.substring(11);
            }
        }
        
    }
    
    if (id == -1) 
        msg.verified = false;
    msg.create_node();
    
    return msg;
}

