function choose(id) {
    if (!$SELECTED) {
        $SELECTED = true;
        function doSubmit() {
            $.ajax({ url: "/submit-selection",
                data: {
                    topic_id: id,
                    screen_name: document.getElementById("tempnamething").value
                },
                error: function(){
                    doSubmit();
                    
                },      
                success: function(data) {
                    window.location.pathname = "/chat";
                }, dataType: "json", type: "POST", timeout: 5000});
        }
        
        doSubmit()
   }
}

window.onload = function() {
    if ($SELECTED) {
        $SELECTED = false;
        choose(-1);
    }
}