var disabled = true;
var captchaChecked = false;

function toggleDisabled() {
    $(".input").prop("disabled", !disabled);
    disabled = !disabled;
}

function verifyLogin() {  
    errorManager.resetDefaults();
    if (!disabled) {
        var fuck = false;

        $("input[type=text], input[type=password]").each(function() {
            if ($(this).val().length == 0) {
                errorManager.giveError($(this).attr("name"), "Field must be completed");
                fuck = true;
            }
        });

        if (fuck)
            return false;

        if ($("input[name=password]").val().length < 6) {
            errorManager.giveError("password", "Valid passwords are 6+ characters");
            return false;
        }
    }
    
    if (!captchaChecked) {
        errorManager.giveError("g-recaptcha", "Don't forget the captcha!");
        return false;
    }

    return true;
}

function checkCaptcha() {
    captchaChecked = true;
    $("#g-recaptcha-popup").remove();
}

$(document).ready(function() {
    errorManager.loadDefaults();
    errorManager.loadErrors();
    
    if (!$("input[name=anon]").is(':checked'))
        toggleDisabled();
});