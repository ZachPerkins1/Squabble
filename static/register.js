function verifyRegister() {  
    errorManager.resetDefaults();
    var fuck = false;
    $("input[type=text], input[type=password]").each(function() {
        if ($(this).val().length == 0) {
            errorManager.giveError($(this).attr("name"), "Field must be completed");
            fuck = true;
        }
    });

    if (fuck)
        return false;

    if ($("input[name=password]").val().length < 6) {
        errorManager.giveError("password", "Valid passwords are 6+ characters");
        return false;
    }
    
    if ($("input[name=password]").val() != $("input[name=password-confirm]").val()) {
        errorManager.giveError("password-confirm", "Password does not match");
        return false;
    }
    
    return true;
}

$(document).ready(function() {
    errorManager.loadDefaults();
    errorManager.loadErrors();
});