var errorManager = {
    giveError: function(elem, error) {
        var $element = null;
        
        if (elem == "g-recaptcha") {
            $element = $(".g-recaptcha");
        } else {
            $element = $("input[name=" + elem + "]"); // The input we are erroring
        
            //Make the input look errory
            $element.css("background-color", "rgb(248, 192, 192)");
            $element.css("border-color", "red");
    
            // Make it so that when you click on the box all the text highlights
            $element.focus(function() {
                $(this).select();
            });
    
            $element.keydown(function() {
                $("#" + elem + "-popup").css("opacity", 0); // Hide pop-up
                $(this).css("border-color", "#9d9d9d");
                $(this).css("background-color", "white");
                $(this).off("focus"); // Get rid of the thing we did earlier
            });
        }
        
        function doResize($popup) {
            $popup.css("top", $element.offset().top + (($element.height() - 35) / 2));
            $popup.css("left", $element.offset().left + $element.width() + 10);
        }
        
        // Clone our master pop-up and rename the id to something findable
        $popup = $("#master-popup").clone();
        $popup.attr("id", elem + "-popup");
        $popup.find(".text").html(error);
    
        // b/c it is absolutely positioned we need to make sure it always gets put back next to it's parent element
        doResize($popup);
        $(window).resize(function() {
            doResize($("#" + elem + "-popup"));
        })
    
        $(document.body).append($popup);
        
    },
    
    resetDefaults: function() {
        $(".popup[id!='master-popup']").each(function() {
            $(this).remove(); 
        });
        
        $("input[type=text], input[type=password]").each(function() {
            $(this).css("border-color", "#9d9d9d");
            $(this).css("background-color", "white");
            $(this).off("focus"); // Get rid of the thing we did earlier
        });
    },
    
    loadDefaults: function() {
        for (var d in $DEFAULTS) {
            var value = $DEFAULTS[d];
            var $field = $("input[name="+ d + "]");
            var type = $field.attr("type");
            if (type == "checkbox") {
                if (value == "on")
                    $field.attr("checked", true);
                else
                    $field.attr("checked", false);
            } else {
                $field.attr("value", value);
            }
        }
        
    },
    
    loadErrors: function() {
        for (var item in $ERRORS)
            this.giveError(item, $ERRORS[item]);
    }
}