# Squabble!

## What is **Squabble**?
Squabble is a semi-anonymous online chat app that exists solely for the purpose of having pointless arguments. Its goal is to cultivate the online existence of flame wars and toxic comment sections.

## How is it written?
### Backend
The backend is written in Python 2.7, using the [Tornado](http://www.tornadoweb.org/en/stable/) framework, developed for asynchronous processes. The database used is [PostgreSQL](https://www.postgresql.org/) and accessed using [Momoko](http://momoko.readthedocs.io/en/latest/), an asynchronous wrapper for [psycopg2](http://initd.org/psycopg). 

### Frontend
The front-end is written using HTML5, CSS3 and Javascript, with use of Jquery and Jquery UI.

### Mobile
To be updated at some point