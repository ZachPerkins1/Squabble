import yaml
import asyncio
import asyncpg
import tornado.web
import os
from squabble.routes import routes

def main():
	settings = None

	with open('settings.yml', 'r') as settings_file:
		settings = yaml.load(settings_file)

	db_settings = settings['database']
	loop = asyncio.get_event_loop()
	
	db = loop.run_until_complete(asyncpg.connect(
		user=db_settings['user'], 
		password=db_settings['password'],
		database=db_settings['name'], 
		host=db_settings['host'],
		port=db_settings['port']
	))

	with open(db_settings['schema']) as db_schema:
		loop.run_until_complete(db.execute(''.join(db_schema.readlines())))

	app = tornado.web.Application(
		routes,
		cookie_secret="THING",
		template_path=os.path.join(os.path.dirname(__file__), "templates"),
		static_path=os.path.join(os.path.dirname(__file__), "static"),
		xsrf_cookies=True,
		debug=settings['development-mode']
	)

	app.db = db

	app.listen(settings['port'])
	tornado.ioloop.IOLoop.current().start()

if __name__ == '__main__':
	main()