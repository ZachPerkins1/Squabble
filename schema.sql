CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    screen_name VARCHAR(64)
);

DROP TABLE IF EXISTS sessions;
CREATE TABLE sessions (
    id VARCHAR(64) PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    user_agent VARCHAR(255),
    ipv4_address VARCHAR(4),
    ipv6_address VARCHAR(16),
    chat_instances INTEGER DEFAULT 0,
    active BOOLEAN DEFAULT TRUE,
    last_active TIMESTAMP DEFAULT CLOCK_TIMESTAMP()
);

DROP TABLE IF EXISTS chats CASCADE;
CREATE TABLE chats (
    id SERIAL NOT NULL PRIMARY KEY,
    created TIMESTAMP DEFAULT CLOCK_TIMESTAMP(),
    topic_id INTEGER DEFAULT 0,
    active BOOLEAN DEFAULT true,
    message_count INTEGER DEFAULT 0,
    last_message INTEGER DEFAULT 0 
);

DROP TABLE IF EXISTS chat_members;
CREATE TABLE chat_members (
    user_id INTEGER REFERENCES users(id),
    chat_id INTEGER REFERENCES chats(id),
    opinion INTEGER
);
CREATE INDEX member_user_index ON chat_members(user_id);
CREATE INDEX member_chat_index ON chat_members(chat_id);

DROP TABLE IF EXISTS messages;
CREATE TABLE messages (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id), 
    chat_id INTEGER REFERENCES chats(id),
    sent TIMESTAMP DEFAULT CLOCK_TIMESTAMP(),
    message TEXT
);

DROP TABLE IF EXISTS topic_sets;
CREATE TABLE topic_sets (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    created TIMESTAMP DEFAULT CLOCK_TIMESTAMP()
);

DROP TABLE IF EXISTS topics;
CREATE TABLE topics (
    id SERIAL NOT NULL PRIMARY KEY,
    topic_set_id INTEGER DEFAULT 1 REFERENCES topic_sets,
    title VARCHAR(255),
    description TEXT,
    user_count INTEGER DEFAULT 0
);