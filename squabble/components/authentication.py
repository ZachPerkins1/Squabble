import re

models = {}

def fetch_columns(table, schema="public"):
	pass

class BaseModelMeta(type):
	def __new__(self, name, inheritance, attr_dict):
		cls = super().__new__(self, name, inheritance, attr_dict)
		if name != 'BaseModel':
			model_name = '_'.join(re.findall('([A-Z][a-z]+)', name)).lower()
			cls._model_name = model_name
			cls._table_name = model_name + 's'
			models[cls._table_name] = cls
			print(models)

		return cls

# Base model is the superclass of all model objects
class BaseModel(object, metaclass=BaseModelMeta):
	def __init__(self, data, create_models=True, autofetch_models=False):
		# Based on our data, we may need to create extra models. i.e. if we've joined
		# the user table to our chat table, we would create a seperate model for the 
		# user. This keeps track of the data that will be in each of those models
		models_to_create = {}

		# Add or change a piece of data for a model in models_to_create. Handles
		# cases where that model is not already a part of models_to_create
		def set_external_model_data(model_cls, column, value):
			if table not in models_to_create:
				models_to_create[model_cls] = {}

			models_to_create[model_cls][column] = value	

		# Actually load those models and add them as attributes for our model
		def load_external_models():
			for model_cls, data in models_to_create.items():
				setattr(self, model_cls._model_name, model_cls(data))

		# For every item in the initial data dictionary we got, check to see if it belongs
		# to this model or another model
		for key, value in data.items():
			table, column = self._table_name, key
			if '.' in key:
				table, column = key.split('.', 1)
				if table != self._table_name and table in models:
					set_external_model_data(models[table], column, value)
				else:
					setattr(self, key.replace('.', '_'), value)
			else:
				setattr(self, column, value)

		load_external_models()

	@classmethod
	async def fetch():


	@classmethod
	async def load():


	@classmethod
	async def save():


	@classmethod
	async def of():

		

	
	
			

class User(BaseModel):
	pass

class Potato(BaseModel):
	pass

u = User({'potatos.': 'jenkins'})

print(u.potato.name_potato)
